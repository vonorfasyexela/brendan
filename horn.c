#include <gst/gst.h>
#include <wiringPi.h>

#define TRIGGER_PIN  0
#define RELAY_PIN    2

int main( int argc, char *argv[] ) {
	
	// Initialize GStreamer
	gst_init(NULL, NULL);

	if (wiringPiSetup() == -1) {
		g_printerr( "Cannot initialize wiringPi\n" );
		return -1;
	}

	g_print( "Waiting for button...\n" );

	// Create elements
	GstElement *pipeline = gst_pipeline_new ("overlay");
	GstElement *src = gst_element_factory_make ("filesrc", "src");
	GstElement *parser = gst_element_factory_make ("wavparse", "parser");
	GstElement *volume = gst_element_factory_make ("volume", "volume");
	GstElement *sink = gst_element_factory_make ("autoaudiosink", "sink");

	if ( !pipeline || !src || !parser || !volume || !sink ) {
		g_printerr("Cannot create elements\n");
		return -1;
	}

	// Add and link
	gst_bin_add_many (GST_BIN (pipeline), src, parser, volume, sink, NULL);
	if ( gst_element_link_many( src, parser, volume, sink, NULL ) != 1) {
		g_printerr( "Cannot link elements\n" );
		return -1;
	}

	// Tune elements
	g_object_set( src, "location", "/home/pi/brendan/horn.wav", NULL );
	g_object_set( volume, "volume", 0.5, NULL );

	GstBus *bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));

	// Setup trigger pin
	pinMode( TRIGGER_PIN, INPUT );
	digitalWrite( RELAY_PIN, LOW );
	pinMode( RELAY_PIN, OUTPUT );
	pullUpDnControl ( TRIGGER_PIN, PUD_DOWN ) ;

	// Wait for button
	while ( 1 ) {
		if ( digitalRead( TRIGGER_PIN ) ) {
			g_print( "Pin has 1 value\n" );
			digitalWrite( RELAY_PIN, HIGH );
			gst_element_set_state (pipeline, GST_STATE_PLAYING);
			gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE, GST_MESSAGE_EOS);
			gst_element_set_state (pipeline, GST_STATE_READY);
			digitalWrite( RELAY_PIN, LOW );
		}
	}
	

	return 0;
}
